package CadenasTexto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio7 {

	//leer cadena de texto con espacio y contar cantidad de palabras(espacio no cuenta);
	
	public static void main(String[] args) {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String a = "";
		int cont = 0;
		
		try {
			System.out.println("Escribe una cadena de texto: ");	
			a = br.readLine();
		} catch (IOException e) {
			
		}
		
		String cadena = a.trim(); //Borrar espacio de detras y delante si los hay		
		
		if(cadena.isEmpty()){
			System.out.println("Cadena vacia");
		}else{	
			if(cadena.indexOf(" ")==-1){
				cont++;
			}else{
				
			}			
			
		}
		
		System.out.println("Palabras: "+cont);
		
	}

}