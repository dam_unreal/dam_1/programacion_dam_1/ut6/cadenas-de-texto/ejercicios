package CadenasTexto;

import java.io.InputStreamReader;

public class Ejercicio5 {

	public static void main(String[] args){
		
		String cadena = "HOHOLAHOLAHOLA";
		String sub = "HO";
		int cont = 0;
		int tamaño = cadena.length() - sub.length();
		
		for(int i=0;i<tamaño;i++){		
			if(cadena.substring(i, i+sub.length()).equalsIgnoreCase(sub)){
				cont++;
				i+=sub.length()-1;
			}	
		}
		
		System.out.println(cont);
	}
}